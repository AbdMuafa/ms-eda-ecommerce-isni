﻿



namespace Payment.Domain.Entities
{
    public class CartsEntity
    {
        public Guid Id { get; set; }
        public Guid CustomerId { get; set; }
        public CartsStatusEnum Status { get; set; } = default!;
        public DateTime Modified { get; internal set; } = DateTime.Now;

    }
}
