﻿

using System.ComponentModel.DataAnnotations.Schema;

namespace Payment.Domain.Entities
{
    public class PaymentEntity
    {
        public Guid Id { get; set; }
        public Guid CartId { get; set; }
        public decimal Total { get; set; }
        public decimal Pay { get; set; }
        public CartsStatusEnum Status { get; set; }
        public PaymentStatusEnum PaymentStatus { get; set; }
        public DateTime Modified { get; internal set; } = DateTime.Now;

        [ForeignKey("CartId")]
        public virtual CartsEntity Cart { get; set; }
    }
}
