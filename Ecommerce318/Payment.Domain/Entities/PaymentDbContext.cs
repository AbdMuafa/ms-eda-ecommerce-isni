﻿

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Payment.Domain.Entities.Configuration;

namespace Payment.Domain.Entities
{
    public class PaymentDbContext : DbContext
    {
        public PaymentDbContext(DbContextOptions<PaymentDbContext> options) : base(options)
        {

        }

        public DbSet<PaymentEntity> Payment { get; set; }
        public DbSet<CartsEntity> Cart { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new PaymentConfiguration());
            modelBuilder.ApplyConfiguration(new CartsConfiguration());

        }

        public static DbContextOptions<PaymentDbContext> OnConfigure()
        {
            var optionBuilder = new DbContextOptionsBuilder<PaymentDbContext>();
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional : true, reloadOnChange: true);
            optionBuilder
                .UseSqlServer(builder
                        .Build()
                        .GetSection("ConnectionStrings")
                        .GetSection("Payment_Db_Conn").Value);

            return optionBuilder.Options;


        }


    }
}
