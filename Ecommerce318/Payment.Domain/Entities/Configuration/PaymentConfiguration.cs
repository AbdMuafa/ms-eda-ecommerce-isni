﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Payment.Domain.Entities.Configuration
{
    public class PaymentConfiguration : IEntityTypeConfiguration<PaymentEntity>
    {
        public void Configure(EntityTypeBuilder<PaymentEntity> builder)
        {
            builder.ToTable("Payment");
            builder.HasKey(p => p.Id);
            builder.Property(p => p.CartId).IsRequired();
            builder.Property(p => p.Total).HasPrecision(18, 2);
            builder.Property(p => p.Pay).HasPrecision(18, 2);
            builder.Property(p => p.Status).IsRequired();
        }
    }

    public class CartsConfiguration : IEntityTypeConfiguration<CartsEntity>
    {
        public void Configure(EntityTypeBuilder<CartsEntity> builder)
        {
            builder.ToTable("Carts");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.CustomerId).IsRequired();
            builder.Property(x => x.Status).IsRequired();
        }
    }
}
