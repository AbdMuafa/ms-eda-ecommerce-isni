﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Payment.Domain
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum CartsStatusEnum
    {
        Pending,
        Confirmed,
        Paid,
        Canceled
    }
}
