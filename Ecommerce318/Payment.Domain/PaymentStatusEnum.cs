﻿

using System.Text.Json.Serialization;

namespace Payment.Domain
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum PaymentStatusEnum
    {
        Pending,
        Paid,
        Cancel
    }
}
