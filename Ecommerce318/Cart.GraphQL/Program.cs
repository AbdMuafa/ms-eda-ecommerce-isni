using Cart.Domain;
using Cart.Domain.MapProfile;
using Cart.Domain.Repositories;
using Cart.Domain.Services;
using Cart.GraphQL.Scema.Mutations;
using Cart.GraphQL.Scema.Queries;
using Cart.GraphQL.Scema.Query;
using Framework.Core.Events;
using Framework.Kafka;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDomainContext(options =>
{
    var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json",
                optional: true, reloadOnChange: true);

    options
        .UseSqlServer(builder.Build()
        .GetSection("ConnectionStrings")
        .GetSection("Cart_Db_Conn").Value);

    options
        .EnableSensitiveDataLogging(false)
        .UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
});


builder.Services.AddControllers();
builder.Services.AddAutoMapper(config =>
{
    config.AddProfile<EntityToDtoProfile>();
});
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddEventBus();
builder.Services.AddKafkaProducerAndConsumer();
builder.Services.AddCart();


builder.Services
    .AddAuthentication(opt =>
    {
        opt.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
        opt.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    })
    .AddJwtBearer("Bearer", opt =>
    {
        var configuration = builder.Configuration;
        opt.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = configuration["JWT:ValidIssuer"],
            ValidAudience = configuration["JWT:ValidAudience"],
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["JWT:Secret"]))
        };
        opt.Events = new JwtBearerEvents
        {
            OnChallenge = context =>
            {
                context.Response.OnStarting(async () =>
                {
                    await context.Response.WriteAsync("Account not authorized");
                });
                return Task.CompletedTask;
            },
            OnForbidden = context =>
            {
                context.Response.OnStarting(async () =>
                {
                    await context.Response.WriteAsync("Account forbidden");
                });
                return Task.CompletedTask;
            }

        };
    });



builder.Services
    .AddScoped<CartQuery>()
    .AddScoped<CartMutation>()
    .AddScoped<CartProductQuery>()
    .AddScoped<CartProductMutation>()
    .AddScoped<ICartsRepository, CartsRepository>()
    .AddScoped<ICartService, CartService>()
    .AddScoped<ICartProductRepository, CartProductsRepository>()
    .AddScoped<ICartProductService, CartProductService>()
    .AddScoped<IProductRepository, ProductRepository>()
    .AddScoped<IProductService, ProductService>()
    .AddGraphQLServer()
    .AddQueryType(q => q.Name("Query"))
    .AddType<CartQuery>()
    .AddType<CartProductQuery>()
    .AddMutationType(m => m.Name("Mutation"))
    .AddType<CartMutation>()
    .AddType<CartProductMutation>()
    .AddAuthorization();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.UseRouting();

app.UseAuthentication();

app.MapControllers();

app.MapGraphQL();

app.Run();
