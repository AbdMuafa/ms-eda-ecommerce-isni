﻿using Cart.Domain.Dtos;
using Cart.Domain.Services;
using HotChocolate.Authorization;

namespace Cart.GraphQL.Scema.Mutations
{
    [ExtendObjectType("Mutation")]
    public class CartProductMutation
    {
        private readonly ICartProductService _service;
        public CartProductMutation(ICartProductService service)
        {
            _service = service;
        }
        [Authorize(Roles = new[] { "customer" })]
        public async Task<CartProductDto> AddCartProductAsync(CartProductTypeInput cartProduct)
        {
            CartProductDto dto = new CartProductDto()
            {
                CartId = cartProduct.CartId,
                ProductId = cartProduct.ProductId,
                Quantity = cartProduct.Quantity
            };
            return await _service.AddCartProduct(dto);
        }
        //[Authorize(Roles = new[] { "customer" })]
        //public async Task<CartProductDto> EditCartProductAsync(Guid id, CartProductTypeInput cartProduct)
        //{
        //    CartProductDto dto = new CartProductDto();
        //    dto.Id = id;
        //    dto.CartId = cartProduct.CartId;
        //    dto.ProductId = cartProduct.ProductId;
        //    //dto.Sku = cartProduct.Sku;
        //    //dto.Name = cartProduct.Name;
        //    dto.Quantity = cartProduct.Quantity;
        //    dto.Price = cartProduct.Price;

        //    var result = await _service.UpdateCartProduct(dto);
        //    if (!result)
        //        throw new GraphQLException(new Error("Cart Product not found, 404"));

        //    return dto;
        //}
    }
}
