﻿using Cart.Domain;
using Cart.Domain.Dtos;
using Cart.Domain.Services;
using HotChocolate.Authorization;

namespace Cart.GraphQL.Scema.Mutations
{
    [ExtendObjectType("Mutation")]
    public class CartMutation
    {
        private readonly ICartService _service;
        public CartMutation(ICartService service)
        {
            _service = service;
        }

        //[Authorize(Roles = new[] { "customer" })]
        public async Task<CartDto> AddCartAsync(Guid customerId)
        {
            CartDto dto = new CartDto();
            dto.CustomerId = customerId;
            return await _service.AddCart(dto);
        }
        //[Authorize(Roles = new[] { "customer" })]
        //public async Task<CartsDto> EditStatusCartAsync(Guid id)
        //{
        //    var result = await _service.UpdateStatusCart(id, CartStatusEnum.Confirmed);
        //    if (!result)
        //        throw new GraphQLException(new Error("Cart not found, 404"));

        //    return await _service.GetCartById(id);
        //}

        public async Task<CartDto> ConfirmCartAsync(Guid id)
        {
            CartDto dto = new CartDto();
            dto.Id = id;
            var result = await _service.UpdateStatusCart(id, CartStatusEnum.Confirmed);
            if (result)
                dto = await _service.GetCartById(id);

            return dto;
        }
    }
}
