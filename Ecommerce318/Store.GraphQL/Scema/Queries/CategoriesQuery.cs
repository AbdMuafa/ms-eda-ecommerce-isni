﻿using HotChocolate.Authorization;
using Store.Domain.Dtos;
using Store.Domain.Services;

namespace Store.GraphQL.Scema.Queries
{
    [ExtendObjectType("Query")]
    public class CategoriesQuery
    {

        private readonly ICategoriesService _services;

        public CategoriesQuery(ICategoriesService services)
        {
            _services = services;
        }

        [Authorize]
        public async Task<IEnumerable<CategoriesDto>> GetAllCategoriesAsync()
        {
            IEnumerable<CategoriesDto> result = await _services.All();
            return result;
        }

        [Authorize]
        public async Task<CategoriesDto> GetCategoryByIdAsync(Guid id)
        {
            return await _services.GetCategoriesById(id);
        }

    }
}
