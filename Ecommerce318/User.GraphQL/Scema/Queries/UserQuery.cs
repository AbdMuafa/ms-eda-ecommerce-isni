﻿using User.Domain.Dtos;
using User.Domain.Services;

namespace User.GraphQL.Scema.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class UserQuery
    {
        private readonly IUserService _service;

        public UserQuery(IUserService service)
        {
            _service = service;
        }

        public async Task<IEnumerable<UserDto>> GetAllDataUserAsync()
        {
            IEnumerable<UserDto> result = await _service.All();
            return result;
        }

        public async Task<UserDto> GetUserById(Guid id)
        {
            UserDto result = await _service.GetUserById(id);
            return result;
        }
    }
}
