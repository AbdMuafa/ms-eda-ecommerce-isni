﻿using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Store.Domain.Entities;
using Store.Domain.Repository;
using Store.Test.MockData;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using Xunit.Abstractions;

namespace Store.Test.Domain.Repository
{
    public class TestCategoryRepository : IDisposable
    {
        protected readonly StoreDbContext _context;
        private readonly ITestOutputHelper _output;

        
        public TestCategoryRepository(ITestOutputHelper output)
        {
            var options = new DbContextOptionsBuilder<StoreDbContext>()
                .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                .Options;

            _context = new StoreDbContext(options);
            _context.Database.EnsureCreated();
            _output = output;
        }

        [Fact]
        public async Task GetAllAsync_ReturnCategoriesCollection()
        {
            //Arrange
            _context.Categories.AddRange(CategoryMockData.GetCategories());     
            _context.SaveChanges();

            var repo = new CategoriesRepository(_context);

            //Action
            var result = await repo.GetAll();
            var count = CategoryMockData.GetCategories().Count();
            _output.WriteLine("c Count : {0}", count);

            //Assret
            result.Should().HaveCount(2);
        }

        [Fact]
        public async Task GetByIdAsync_ReturnCategoriesCollection()
        {
            //Arrange
            _context.Categories.AddRange(CategoryMockData.GetCategories());
            _context.SaveChanges();

            var repo = new CategoriesRepository(_context);

            //Action
            var result = await repo.GetById(new Guid("CA9F4E20-FA43-4E85-9762-32460022DE19"));

            //Assret
            result.Name.Should().Be("Food");
        }

        public void Dispose()
        {;
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}
