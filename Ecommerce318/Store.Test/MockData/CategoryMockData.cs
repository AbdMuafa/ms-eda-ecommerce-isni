﻿using Store.Domain;
using Store.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Test.MockData
{
    public class CategoryMockData
    {
        public static List<CategoriesEntity> GetCategories()
        {
            return new List<CategoriesEntity>()
            {
                new CategoriesEntity()
                {
                    Id = new Guid("AC7D944C-6E84-4A2F-A47F-A8B4BAC1ED89"),
                    Name = "Food",
                    Description = "Makanan Utama",
                    Status = StoreStatusEnum.Active
                },
                new CategoriesEntity
                {
                    Id = new Guid("CA9F4E20-FA43-4E85-9762-32460022DE19"),
                    Name = "Food",
                    Description = "Makanan Utama",
                    Status = StoreStatusEnum.Inactive
                }
            };
        }
    }
}
