﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store.Domain.EventEnvelope.Product
{
    public record ProductCreated
    (
        Guid? Id,
        string Sku,
        string Name,
        decimal Price,
        decimal Volume,
        int Sold,
        int Stock,
        StoreStatusEnum Status
    )
    {
        public static ProductCreated Create(
            Guid id,
            string sku,
            string name,
            decimal price,
            decimal volume,
            int sold,
            int stock,
            StoreStatusEnum status
            ) => new(id, sku, name, price, volume, sold, stock, status);
    }
}

//store consume attribute from lookup
