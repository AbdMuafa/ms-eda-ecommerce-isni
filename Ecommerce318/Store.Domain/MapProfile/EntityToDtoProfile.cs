﻿using AutoMapper;
using Store.Domain.Dtos;
using Store.Domain.Entities;

namespace Store.Domain.MapProfile
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile() : base("Entity to Dto Profile")
        {
            CreateMap<CategoriesEntity, CategoriesDto>();
            CreateMap<CategoriesDto, CategoriesEntity>();

            CreateMap<ProductEntity, ProductDto>();
            CreateMap<ProductDto, ProductEntity>();
        }
    }
}
