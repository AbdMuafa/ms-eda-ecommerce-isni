﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Store.Domain.Entities.Configuration
{
    public class CategoriesConfiguration : IEntityTypeConfiguration<CategoriesEntity>
    {
        public void Configure(EntityTypeBuilder<CategoriesEntity> builder)
        {
            builder.ToTable("Categories");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Name).HasMaxLength(50).IsRequired();
            builder.Property(e => e.Description).HasMaxLength(255).IsRequired();
            builder.Property(e => e.Status).IsRequired();  
        }
    }

    public class ProductConfiguration : IEntityTypeConfiguration<ProductEntity>
    {
        public void Configure(EntityTypeBuilder<ProductEntity> builder)
        {
            builder.ToTable("Products");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.CategoryId).IsRequired();
            builder.Property(e => e.AttributeId).IsRequired();
            builder.Property(e => e.Sku).HasMaxLength(20).IsRequired();
            builder.Property(e => e.Name).HasMaxLength(50).IsRequired();
            builder.Property(e => e.Description).HasMaxLength(255).IsRequired();
            builder.Property(e => e.Price).HasPrecision(18,2);
            builder.Property(e => e.Volume).HasPrecision(18, 2);
            builder.Property(e => e.Status).IsRequired();
        }
    }

    public class AttributeConfiguration : IEntityTypeConfiguration<AttributeEntity>
    {
        public void Configure(EntityTypeBuilder<AttributeEntity> builder)
        {
            builder.ToTable("Attributes");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.Type).IsRequired();
            builder.Property(e => e.Unit).HasMaxLength(30).IsRequired();
            builder.Property(e => e.Status).IsRequired();
        }
    }


}
