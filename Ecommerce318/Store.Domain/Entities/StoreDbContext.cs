﻿using Microsoft.EntityFrameworkCore;
using Store.Domain.Entities.Configuration;
using Microsoft.Extensions.Configuration;

namespace Store.Domain.Entities
{
    public class StoreDbContext : DbContext
    {
        public StoreDbContext(DbContextOptions<StoreDbContext> options) : base(options)
        {

        }

        public DbSet<CategoriesEntity> Categories { get; set; }
        public DbSet<ProductEntity> Products { get; set; }
        public DbSet<AttributeEntity> Attributes { get; set; }


        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CategoriesConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new AttributeConfiguration());
        }

        public static DbContextOptions<StoreDbContext> OnConfigure()
        {
            var optionsBuilder = new DbContextOptionsBuilder<StoreDbContext>();
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            optionsBuilder
                .UseSqlServer(builder
                                .Build()
                                .GetSection("ConnectionStrings")
                                .GetSection("Store_Db_Conn").Value);

            return optionsBuilder.Options;
        }

    }
}