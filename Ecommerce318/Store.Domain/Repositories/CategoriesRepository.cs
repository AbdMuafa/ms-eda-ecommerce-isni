﻿using Microsoft.EntityFrameworkCore;
using Store.Domain.Entities;


namespace Store.Domain.Repository
{
    public interface ICategoriesRepository
    {
        Task<int> GetCount();
        Task<IEnumerable<CategoriesEntity>> GetAll();
        Task<IEnumerable<CategoriesEntity>> GetPaged(int page, int size);
        Task<CategoriesEntity> GetById(Guid id);
        Task<CategoriesEntity> Add(CategoriesEntity entity);
        Task<CategoriesEntity> Update(CategoriesEntity entity);
        void Delete(CategoriesEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }

    public class CategoriesRepository : ICategoriesRepository
    {
        protected readonly StoreDbContext _context;

        public CategoriesRepository(StoreDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }
        public async Task<CategoriesEntity> Add(CategoriesEntity entity)
        {
            _context.Set<CategoriesEntity>().Add(entity);
            return entity;
        }

        public void Delete(CategoriesEntity entity)
        {
            _context.Set<CategoriesEntity>().Update(entity);
        }

        public async Task<IEnumerable<CategoriesEntity>> GetAll()
        {
            return await _context.Set<CategoriesEntity>().ToListAsync();
        }

        public async Task<CategoriesEntity> GetById(Guid id)
        {
            return await _context.Set<CategoriesEntity>().FindAsync(id);
        }

        public Task<int> GetCount()
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<CategoriesEntity>> GetPaged(int page, int size)
        {
            throw new NotImplementedException();
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<CategoriesEntity> Update(CategoriesEntity entity)
        {
            _context.Set<CategoriesEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
