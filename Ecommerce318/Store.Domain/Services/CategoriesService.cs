﻿

using AutoMapper;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.Repository;

namespace Store.Domain.Services
{
    public interface ICategoriesService
    {
        Task<IEnumerable<CategoriesDto>> All();
        Task<CategoriesDto> GetCategoriesById(Guid id);
        Task<CategoriesDto> AddCategories(CategoriesDto dto);
        Task<bool> UpdateCategories(CategoriesDto dto);
        Task<bool> UpdateStatus(Guid id, StoreStatusEnum status);

    }

    public class CategoriesService : ICategoriesService
    {
        private ICategoriesRepository _repository;
        private readonly IMapper _mapper;

        public CategoriesService(ICategoriesRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }
        public async Task<CategoriesDto> AddCategories(CategoriesDto dto)
        {
            if (dto != null)
            {
                dto.Status = StoreStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<CategoriesEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                    return _mapper.Map<CategoriesDto>(entity);
            }
            return new CategoriesDto();
        }

        public async Task<IEnumerable<CategoriesDto>> All()
        {
            return _mapper.Map<IEnumerable<CategoriesDto>>(await _repository.GetAll());
        }

        public async Task<CategoriesDto> GetCategoriesById(Guid id)
        {
            return _mapper.Map<CategoriesDto>(await _repository.GetById(id));
        }

        public async Task<bool> UpdateCategories(CategoriesDto dto)
        {
            if (dto != null)
            {
                var category = await _repository.GetById(dto.Id);
                dto.Status = category.Status;
                if (category != null)
                {
                    await _repository.Update(_mapper.Map<CategoriesEntity>(dto));
                    var result = await _repository.SaveChangesAsync();
                    if (result > 0)
                        return true;
                }
            }
            return false;
        }

        public async Task<bool> UpdateStatus(Guid id, StoreStatusEnum status)
        {
            var category = await _repository.GetById(id);
            if (category != null)
            {
                category.Status = status;
                var entity = await _repository.Update(category);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                    return true;
            }

            return false;
        }
    }
}

