﻿using AutoMapper;
using Framework.Core.Events;
using Framework.Core.Events.Externals;
using Store.Domain.Dtos;
using Store.Domain.Entities;
using Store.Domain.EventEnvelope.Product;
using Store.Domain.Repositories;


namespace Store.Domain.Services
{
    public interface IProductService
    {
        Task<IEnumerable<ProductDto>> All();
        Task<ProductDto> GetProductById(Guid id);
        Task<ProductDto> AddProduct(ProductDto dto);
        Task<bool> UpdateProduct(ProductDto dto);
        Task<bool> UpdateStatus(Guid id, StoreStatusEnum status);
        Task<bool> RemoveProduct(Guid id);
    }

    public class ProductService : IProductService
    {
        private readonly IProductRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public ProductService(IProductRepository repository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }

        public async Task<ProductDto> AddProduct(ProductDto dto)
        {
            if(dto != null)
            {
                dto.Status = StoreStatusEnum.Inactive;
                var dtoToEntity = _mapper.Map<ProductEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if(result > 0)
                {
                    var externalEvent = new EventEnvelope<ProductCreated>(
                        ProductCreated.Create(
                            entity.Id,
                            entity.Sku,
                            entity.Name,
                            entity.Price,
                            entity.Volume,
                            entity.Sold,
                            entity.Stock,
                            entity.Status)
                        );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                    return _mapper.Map<ProductDto>(entity);
                }
            }
            return new ProductDto();
        }

        public async Task<IEnumerable<ProductDto>> All()
        {
            return _mapper.Map<IEnumerable<ProductDto>>(await _repository.GetAll());
        }

        public async Task<ProductDto> GetProductById(Guid id)
        {
            return _mapper.Map<ProductDto>(await _repository.GetById(id)); 
        }

        public async Task<bool> UpdateProduct(ProductDto dto)
        {
            if(dto != null)
            {
                var product = await _repository.GetById(dto.Id);
                dto.Status = product.Status;
                var entity = await _repository.Update(_mapper.Map<ProductEntity>(dto));
                var result = await _repository.SaveChangesAsync();
                if(product != null)
                {
                    if (result > 0)
                    {

                        var externalEvent = new EventEnvelope<ProductUpdated>(
                        ProductUpdated.UpdateData(
                            entity.Id,
                            entity.CategoryId,
                            entity.AttributeId,
                            entity.Sku,
                            entity.Name,
                            entity.Description,
                            entity.Price,
                            entity.Volume,
                            entity.Sold,
                            entity.Stock
                            )
                        );
                        await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                        return true;
                    }
                }
            }
            return false;
        }

        public async Task<bool> UpdateStatus(Guid id, StoreStatusEnum status)
        {
            var product = await _repository.GetById(id);
            if(product != null)
            {
                product.Status = status;
                var entity = await _repository.Update(product);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    var ExternalEven = new EventEnvelope<ProductUpdateStatus>(
                        ProductUpdateStatus.UpdateStatus(
                            entity.Id,
                            entity.Status
                            ));
                    await _externalEventProducer.Publish(ExternalEven, new CancellationToken());
                    return true;
                }
            }

            return false;
        }

        public async Task<bool> RemoveProduct(Guid id)
        {
            if (id != null)
            {
                var product = await _repository.GetById(id);
                product.Status = StoreStatusEnum.Removed;
                _repository.Delete(product);
                var result = await _repository.SaveChangesAsync();
                if (result != null)
                {
                    if (result > 0)
                        return true;
                }
            }
            return false;
        }
    }
}
