﻿using Framework.Core.Projection;
using Microsoft.Extensions.DependencyInjection;
using Store.Domain.Projections;

namespace Store.Domain
{
    public static class StoreServices
    {
        public static IServiceCollection AddStore(this IServiceCollection services) =>
            services.AddProjections();

        private static IServiceCollection AddProjections(this IServiceCollection services) =>
            services //.AddScoped<AttributeProjection>();
            .Projection(builder => builder
                .AddOn<AttributeCreated>(AttributeProjection.Handle)
                .AddOn<AttributeUpdated>(AttributeProjection.Handle)
                .AddOn<AttributeStatusChanged>(AttributeProjection.Handle)
            );
    }
}
