﻿using Microsoft.EntityFrameworkCore;
using Cart.Domain.Entities.Configuration;
using Microsoft.Extensions.Configuration;

namespace Cart.Domain.Entities
{
    public class CartDbContext :   DbContext
    {
        public CartDbContext(DbContextOptions<CartDbContext> options) : base(options)
        {

        }

        public DbSet<CartsEntity> Carts { get; set; }
        public DbSet<CartProductEntity> CartProducts { get; set; }
        public DbSet<ProductEntity> Products { get; set; }
        public DbSet<UsersEntity> Users { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CartsConfiguration());
            modelBuilder.ApplyConfiguration(new CartProductsConfiguration());
            modelBuilder.ApplyConfiguration(new ProductsConfiguration());
            modelBuilder.ApplyConfiguration(new UsersConfiguration());
        }

        public static DbContextOptions<CartDbContext> OnConfigure()
        {
            var optionsBuilder = new DbContextOptionsBuilder<CartDbContext>();
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);
            optionsBuilder
                .UseSqlServer(builder
                                .Build()
                                .GetSection("ConnectionStrings")
                                .GetSection("Cart_Db_Conn").Value);

            return optionsBuilder.Options;
        }
    }
}
