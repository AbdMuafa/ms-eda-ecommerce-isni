﻿

using System.ComponentModel.DataAnnotations.Schema;

namespace Cart.Domain.Entities
{
    public class CartProductEntity
    {
        public Guid Id { get; set; }
        public Guid CartId { get; set; }
        public Guid ProductId { get; set; }
        //public string Sku { get; set; } = default!;
        //public string Name { get; set; } = default!;
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public DateTime Modified { get; internal set; } = DateTime.Now;

        [ForeignKey("CartId")]
        public virtual CartsEntity Cart { get; set; }

        [ForeignKey("ProductId")]
        public virtual ProductEntity Product { get; set; }

    }
}
