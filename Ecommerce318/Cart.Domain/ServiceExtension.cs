﻿
using Cart.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Cart.Domain
{
    public static class ServiceExtention
    {
        public static void AddDomainContext(this IServiceCollection services,
            Action<DbContextOptionsBuilder> optionAction,
            ServiceLifetime contextLifetime = ServiceLifetime.Scoped,
            ServiceLifetime optionLifetime = ServiceLifetime.Scoped)
        {
            services.AddDbContext<CartDbContext>(optionAction, contextLifetime,
                optionLifetime);
        }
    }
}
