﻿

namespace Cart.Domain.Dtos
{
   public class ProductDto
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Sku { get; set; }
        public decimal Price { get; set; }
        public decimal Volume { get; set; }
        public int Stock { get; set; }
        public int Sold { get; set; }
        public StoreStatusEnum Status { get; set; }

    }
}
