﻿using AutoMapper;
using Cart.Domain.Dtos;
using Cart.Domain.Entities;
using Cart.Domain.Repositories;
using Framework.Core.Events.Externals;
using Cart.Domain.Repositories;

namespace Cart.Domain.Services
{
    public interface ICartProductService
    {
        Task<IEnumerable<CartProductDto>> All();
        Task<CartProductDto> GetCartProductById(Guid id);
        Task<CartProductDto> AddCartProduct(CartProductDto dto);
        Task<bool> UpdateCartProduct(CartProductDto dto);
    }

    public class CartProductService : ICartProductService
    {
        private readonly ICartProductRepository _cartProductRepository;
        private readonly IProductRepository _productRepository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer externalEventProducer;
        public CartProductService(IProductRepository productRepository, ICartProductRepository cartProductRepository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _cartProductRepository = cartProductRepository;
            _productRepository = productRepository;
            _mapper = mapper;
        }
        public async Task<CartProductDto> AddCartProduct(CartProductDto dto)
        {
            var product = await _productRepository.GetById(dto.ProductId);

            if (product == null)
                return null;

            dto.Price = product.Price;

            var existCp = await _cartProductRepository.GetByCartId(dto.CartId, dto.ProductId);

            if(existCp != null)
            {

                if (product.Stock >= dto.Quantity + existCp.Quantity)
                    existCp.Quantity = dto.Quantity + existCp.Quantity;
                else
                    existCp.Quantity = product.Stock;

                var entity = await _cartProductRepository.Add(_mapper.Map<CartProductEntity>(dto));
                var result = await _cartProductRepository.SaveChangesAsync();

                if (result > 0)
                    return _mapper.Map<CartProductDto>(entity);
            }
            else
            {
                var entity = await _cartProductRepository.Add(_mapper.Map<CartProductEntity>(dto));
                var result = await _cartProductRepository.SaveChangesAsync();
                if(result > 0)
                    return _mapper.Map<CartProductDto>(entity);
                    
            }
            return new CartProductDto();
        }

        public async Task<IEnumerable<CartProductDto>> All()
        {
            return _mapper.Map<IEnumerable<CartProductDto>>(await _cartProductRepository.GetAll());
        }

        public async Task<CartProductDto> GetCartProductById(Guid id)
        {
            return _mapper.Map<CartProductDto>(await _cartProductRepository.GetById(id));
        }

        public async Task<bool> UpdateCartProduct(CartProductDto dto)
        {
            if(dto != null)
            {
                var cartproduct = await _cartProductRepository.GetById(dto.Id);
                await _cartProductRepository.Update(_mapper.Map<CartProductEntity>(dto));
                var result = await _cartProductRepository.SaveChangesAsync();
                if(cartproduct != null)
                {
                    if(result>0)
                        return true;
                }
            }
            return false;
        }
    }
}
