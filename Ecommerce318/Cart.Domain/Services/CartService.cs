﻿using AutoMapper;
using Cart.Domain.Dtos;
using Cart.Domain.Entities;
using Cart.Domain.EvenEnvelopes.Cart;
using Cart.Domain.Repositories;
using Framework.Core.Events;
using Framework.Core.Events.Externals;

namespace Cart.Domain.Services
{
    public interface ICartService
    {
        Task<IEnumerable<CartDto>> All();
        Task<IEnumerable<CartsEntity>> AllIn();
        Task<CartDto> GetCartById(Guid id);
        Task<CartDto> AddCart(CartDto dto);
        Task<bool> UpdateStatusCart(Guid id, CartStatusEnum status);
    }

    public class CartService : ICartService
    {
        private ICartProductRepository _cartProductRepository;
        private ICartsRepository _repository;
        private readonly IMapper _mapper;
        private readonly IExternalEventProducer _externalEventProducer;

        public CartService(ICartProductRepository cartProductRepository, ICartsRepository repository, IMapper mapper, IExternalEventProducer externalEventProducer)
        {
            _cartProductRepository = cartProductRepository;
            _repository = repository;
            _mapper = mapper;
            _externalEventProducer = externalEventProducer;
        }
        public async Task<CartDto> AddCart(CartDto dto)
        {
            if(dto != null)
            {
                dto.Status = CartStatusEnum.Pending;
                var dtoToEntity = _mapper.Map<CartsEntity>(dto);
                var entity = await _repository.Add(dtoToEntity);
                var result = await _repository.SaveChangesAsync();

                if(result > 0)
                {
                    return _mapper.Map<CartDto>(entity);
                }                
            }
            return new CartDto();
        }

        public async Task<IEnumerable<CartDto>> All()
        {
            return _mapper.Map<IEnumerable<CartDto>>(await _repository.GetAll());
        }

        public async Task<CartDto> GetCartById(Guid id)
        {
            return _mapper.Map<CartDto>(await _repository.GetById(id));
        }

    

        public async Task<bool> UpdateStatusCart(Guid id, CartStatusEnum status)
        {
            bool res = true;
            var entity = await _repository.GetById(id);
            if(entity != null)
            {
                entity.Status = status;
                var cart = await _repository.Update(entity);
                var result = await _repository.SaveChangesAsync();

                if (result > 0)
                {
                    List<CartProductItem> list = new List<CartProductItem>();
                    
                    if(status == CartStatusEnum.Confirmed)
                    {

                        var cartProducts = await _cartProductRepository.GetByCartId(entity.Id);

                        foreach(var item in cartProducts)
                        {
                            list.Add(new CartProductItem()
                            {
                                Id = item.Id,
                                ProductId = item.ProductId,
                                Quantity = item.Quantity
                            });
                        }
                    }

                    var externalEvent = new EventEnvelope<CartStatusChanged>(
                        CartStatusChanged.Create(
                            cart.Id,
                            cart.CustomerId,
                            list,
                            cart.Status
                            )
                        );
                    await _externalEventProducer.Publish(externalEvent, new CancellationToken());
                }
            }
            return res;
        }

        public async Task<IEnumerable<CartsEntity>> AllIn()
        {
            return await _repository.GetAll();
        }
    }
}
