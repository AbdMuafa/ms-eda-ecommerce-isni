﻿using Cart.Domain.Projections;
using Framework.Core.Projection;
using Microsoft.Extensions.DependencyInjection;



namespace Cart.Domain
{
    public static class CartServices
    {
        public static IServiceCollection AddCart(this IServiceCollection services)
            => services.AddProjections();

        private static IServiceCollection AddProjections(this IServiceCollection services)
            => services
            .Projection(builder => builder
                .AddOn<UserCreated>(UserProjection.Handle)
                //.AddOn<UserUpdate>(UserProjection.Handle)
                .AddOn<ProductCreated>(ProductProjection.Handle)
                //.AddOn<ProductUpdated>(ProductProjection.Handle)
                //.AddOn<ProductUpdateStatus>(ProductProjection.Handle)
            );
    }
}
