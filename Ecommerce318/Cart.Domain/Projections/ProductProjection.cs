﻿using Cart.Domain.Entities;
using Framework.Core.Events;


namespace Cart.Domain.Projections
{
    public record ProductCreated(
        Guid? Id,
        string Sku,
        string Name,
        decimal Price,
        decimal Volume,
        int Sold,
        int Stock,
        StoreStatusEnum Status
    );

    public class ProductProjection
    {
        public static bool Handle(EventEnvelope<ProductCreated> eventEnvelope)
        {
            var (id, sku, name, price, volume, sold, stock, status) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                ProductEntity entity = new ProductEntity()
                {
                    Id = (Guid)id,
                    Sku = sku,
                    Name = name,
                    Price = price,
                    Volume = volume,
                    Sold = sold,
                    Stock = stock,
                    Status = status
                };

                context.Products.Add(entity);
                context.SaveChanges();
            }
            return true;
        }
    }
}
