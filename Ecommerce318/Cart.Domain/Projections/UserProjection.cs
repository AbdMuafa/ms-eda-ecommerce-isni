﻿


using Cart.Domain.Entities;
using Framework.Core.Events;

namespace Cart.Domain.Projections
{

    public record UserCreated(
        Guid? Id,
        string FirstName,
        string LastName,
        string Email
    );



    public class UserProjection
    {

        public static bool Handle(EventEnvelope<UserCreated> eventEnvelope)
        {
            var (id, firstName, lastName, email) = eventEnvelope.Data;
            using (var context = new CartDbContext(CartDbContext.OnConfigure()))
            {
                UsersEntity entity = new UsersEntity()
                {
                    Id = (Guid)id,
                    FirstName = firstName,
                    LastName = lastName,
                    Email = email
                };

                context.Users.Add(entity);
                context.SaveChanges();
            }
            return true;
        }
    }
}
