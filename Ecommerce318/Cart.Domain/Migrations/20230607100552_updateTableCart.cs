﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Cart.Domain.Migrations
{
    /// <inheritdoc />
    public partial class updateTableCart : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "StatusCart",
                table: "Carts",
                newName: "Status");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Status",
                table: "Carts",
                newName: "StatusCart");
        }
    }
}
