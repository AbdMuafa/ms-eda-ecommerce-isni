﻿using Cart.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace Cart.Domain.Repositories
{
    public interface ICartsRepository
    {
        Task<IEnumerable<CartsEntity>> GetAll();
        Task<CartsEntity> GetById(Guid id);
        Task<CartsEntity> Add(CartsEntity entity);
        Task<CartsEntity> Update(CartsEntity entity);
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
    }

    public class CartsRepository : ICartsRepository
    {
        protected readonly CartDbContext _context;

        public CartsRepository(CartDbContext context)
        {
            _context = context;
            _context.Database.EnsureCreated();
        }

        public async Task<CartsEntity> Add(CartsEntity entity)
        {
            _context.Set<CartsEntity>().AddAsync(entity);
            return entity;
        }

        public async Task<IEnumerable<CartsEntity>> GetAll()
        {
            return await _context.Set<CartsEntity>().ToListAsync();
        }

        public async Task<CartsEntity> GetById(Guid id)
        {
            return await _context.Set<CartsEntity>().FindAsync(id);
        }

        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        public async Task<CartsEntity> Update(CartsEntity entity)
        {
            _context.Set<CartsEntity>().Update(entity);
            return entity;
        }

        public virtual void Dispose(bool disposing)
        {
            if (disposing)
                _context.Dispose();
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
