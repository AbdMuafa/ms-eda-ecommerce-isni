﻿using AutoMapper;
using Cart.Domain.Dtos;
using Cart.Domain.Entities;


namespace Cart.Domain.MapProfile
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile() : base("Entity to Dto Profile")
        {
            CreateMap<CartsEntity, CartDto>();
            CreateMap<CartDto, CartsEntity>();

            CreateMap<CartProductEntity, CartProductDto>();
            CreateMap<CartProductDto, CartProductEntity>();

            CreateMap<ProductDto, ProductEntity>().ReverseMap();
        }
    }
}
