﻿

using HotChocolate.Authorization;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUp.GraphQL.Scema.Queries
{
    [ExtendObjectType(Name = "Query")]
    public class AttributeQuery
    {
        private readonly IAttributeService _services;

        public AttributeQuery(IAttributeService service)
        {
            _services = service;
        }

        [Authorize]
        public async Task<IEnumerable<AttributeDto>> GetAllAttributeAsync()
        {
            IEnumerable<AttributeDto> result = await _services.All();
            return result;
        }

        [Authorize]
        public async Task<AttributeDto> GetAttributeByIdAsync(Guid id)
        {
            AttributeDto result = await _services.GetAttributeById(id);
            return result;            
        }
    }
}
