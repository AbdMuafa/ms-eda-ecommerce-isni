﻿using HotChocolate.Authorization;
using LookUp.Domain;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUp.GraphQL.Scema.Mutations
{
    [ExtendObjectType("Mutation")]
    public class CurrenciesMutation
    {
        private readonly ICurrenciesService _service;

        public CurrenciesMutation(ICurrenciesService service)
        {
            _service = service;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CurrenciesDto> AddCurrenciesAsync(CurrenciesTypeInput currencies)
        {
            CurrenciesDto dto = new CurrenciesDto();
            dto.Name = currencies.Name;
            dto.Code = currencies.Code;
            dto.Symbol = currencies.Symbol;
            var result = await _service.AddCurrencies(dto);
            return result;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CurrenciesDto> UpdateCurrenciesAsync(Guid id, CurrenciesTypeInput currencies)
        {
            CurrenciesDto dto = new CurrenciesDto();
            dto.Id = id;
            dto.Name = currencies.Name;
            dto.Code= currencies.Code;
            dto.Symbol = currencies.Symbol;
            var result = await _service.UpdateCurrencies(dto);
            if (!result)
            {
                throw new GraphQLException(new Error("Currencies not found, 404"));
            }
            return dto;
        }

        [Authorize(Roles = new[] { "admin" })]
        public async Task<CurrenciesDto> UpdateStatusCurrenciesAsync(Guid id, LookUpStatusEnum status)
        {
            var result = await _service.UpdateStatus(id, status);

            if (!result)
            {
                throw new GraphQLException(new Error("Currencies not found, 404"));
            }
            return await _service.GetCurrenciesById(id);
        }
    }
}
