﻿using LookUp.Domain.Dtos;
using FluentValidation;


namespace LookUp.Domain.Validations
{
    public class AttributeValidator : AbstractValidator<AttributeDto>
    {
        public AttributeValidator()
        {
            //Type cannot be null or empty and must consist of Enum
            //Unit must consist atleast 5 characters

            RuleFor(x => x.Type).IsInEnum().WithMessage("Type cannot be null or empty and must consist of Enum");
            RuleFor(x => x.Unit).MinimumLength(4).WithMessage("Unit minimum 4 chars");
            
        }
    }
}
