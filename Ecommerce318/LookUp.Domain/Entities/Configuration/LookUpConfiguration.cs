﻿
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace LookUp.Domain.Entities.Configuration
{
    public class AttributeConfiguration : IEntityTypeConfiguration<AttributeEntity>
    {
        public void Configure(EntityTypeBuilder<AttributeEntity> builder)
        {
            builder.ToTable("Attributes");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.Type).IsRequired();
            builder.Property(e => e.Unit).HasMaxLength(30).IsRequired();
            builder.Property(e => e.Status).IsRequired();
        }
    }

    public class CurrenciesConfiguration : IEntityTypeConfiguration<CurrenciesEntity>
    {
        public void Configure(EntityTypeBuilder<CurrenciesEntity> builder)
        {
            builder.ToTable("Currencies");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Id).IsRequired();
            builder.Property(e => e.Name).HasMaxLength(30).IsRequired();
            builder.Property(e => e.Code).HasMaxLength(3).IsRequired();
            builder.Property(e => e.Symbol).HasMaxLength(3).IsRequired();
            builder.Property(e => e.Status).IsRequired();
        }
    }

}
